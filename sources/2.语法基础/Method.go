// 包名（命名空间）
package main

// 依赖
import "fmt"

func methodName(arg1 int, arg2 int) int {
	return (arg1 * arg2)
}

func getStudentInfo() (string, int) {
	return "张飞", 28
}

// 入口函数
func main() {
	fmt.Printf("结果 %d \n", methodName(1, 2))
	name, age := getStudentInfo()
	// 或
	// var name, age = getStudentInfo()

	fmt.Printf("姓名 %s \n", name)
	fmt.Printf("年龄 %d \n", age)
}
