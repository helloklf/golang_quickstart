// 包名（命名空间）
package main

// 依赖
import (
	"fmt"
)

// 入口函数
func main() {
	// 类似Java中 Map<String, String> mapData;
	var mapData map[string]string // mapData 现在是null！！！
	// 小声哔哔：为什么Array声明完就能用了，map声明完又是一个Null，Go语言设计者是魔鬼吗！！？

	// 初始化map
	mapData = make(map[string]string)
	mapData["name"] = "张飞"
	mapData["address"] = "中国"
	fmt.Printf(mapData["name"] + "\n")

	delete(mapData, "name") // 删除某个值
	for key := range mapData {
		fmt.Println(key, mapData[key])
	}
}
