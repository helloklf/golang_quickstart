// 包名（命名空间）
package main

// 依赖
import "fmt"

var (
	globalVar  int = 1
	globalVar2 int
	globalVa3  = true
)

// 入口函数
func main() {
	var value1, value2, value3 = true, "测试", 12
	fmt.Println(value1)
	fmt.Println(value2)
	fmt.Println(value3)

	value4, value5, value6 := "周一", 2, false
	fmt.Println(value4)
	fmt.Println(value5)
	fmt.Println(value6)

	var (
		value7 int = 7
		value8 int
		value9 = true
	)
	value8 = 8
	fmt.Println(value7)
	fmt.Println(value8)
	fmt.Println(value9)
}
