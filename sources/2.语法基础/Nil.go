// 包名（命名空间）
package main

// 依赖
import "fmt"
import "strconv"

// 入口函数
func main() {
	// var value int = 1
	var value bool
	fmt.Printf(strconv.Formatebool(value))
}