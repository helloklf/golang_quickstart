// 包名（命名空间）
package main

// 依赖
import "fmt"

// 入口函数
func main() {
	// var value int = 1
	var value = true
	if value {
		fmt.Println("真的很")
	} else {
		fmt.Println("假的把")
	}
}
