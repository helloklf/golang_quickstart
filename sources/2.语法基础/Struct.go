// 包名（命名空间）
package main

// 依赖
import "fmt"

type Student struct {
	age  int
	name string
}

// 入口函数
func main() {
	var student1 Student // 真的不用new，student1也真的不是null
	student1.age = 16
	student1.name = "赵云"
	fmt.Printf("赵云年龄 %d ", student1.age)

	var student = Student{18, "张飞"}
	fmt.Printf("张飞年龄 %d ", student.age)

	var student2 = Student{age: 22, name: "关羽"}
	fmt.Printf("关羽年龄 %d ", student2.age)
}
