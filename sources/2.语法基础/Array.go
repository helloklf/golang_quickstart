// 包名（命名空间）
package main

// 依赖
import "fmt"

// 入口函数
func main() {
	var intArr = [...]int{10, 20, 30}
	for j := 0; j < 3; j++ {
		fmt.Printf("Element[%d] = %d\n", j, intArr[j])
	}
	fmt.Printf("数组长度：%d\n", len(intArr))
}
