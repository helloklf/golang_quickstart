// 包名（命名空间）
package main

// 依赖
import "fmt"

// 入口函数
func main() {
	/* for 循环 */
	for a := 0; a < 10; a++ {
		fmt.Printf("a 的值为: %d\n", a)
	}

	var i = 1
	for i < 10 {
		fmt.Printf("i 的值为: %d\n", i)
		i++
	}

	var arr = [4]int{1, 2, 3, 4}
	for index, item := range arr {
		fmt.Printf("item %d 的值为: %d\n", index, item)
	}
}
