package main

import (
	"fmt"
	"time"
)

func HelloWorld (ch (chan string)) {
	time.Sleep(time.Duration(2)*time.Second) // 暂停2秒，模拟耗时操作

	ch <- "Hello ...." // 向channel写入数据
	ch <- "World ...." // 向channel写入数据
	ch <- "！" // 向channel写入数据
}

func main() {
	ch := make(chan string, 5) // 创建channel

	go HelloWorld(ch) // 创建 goroutine

	result := <- ch // 从channel读取数据，在有数据可读取前，会一直等待
	
	close(ch) // 关闭channel（不能重复关闭）

  fmt.Println(result)
}
