package main

import (
	"fmt"
	"time"
)

func HelloWorld (ch (chan string)) {
	time.Sleep(time.Duration(2)*time.Second) // 暂停2秒，模拟耗时操作

	ch <- "HelloWorld ...." // 向channel写入数据
}

func main() {
	ch := make(chan string) // 创建channel

	go HelloWorld(ch) // 创建 goroutine

 result := <- ch // 从channel读取数据，在有数据可读取前，会一直等待

	/*
	关闭一个未初始化(nil) 的 channel 会产生 panic
	重复关闭同一个 channel 会产生 panic
	向一个已关闭的 channel 中发送消息会产生 panic
	从已关闭的 channel 读取消息不会产生 panic，且能读出 channel 中还未被读取的消息，若消息均已读出，则会读到类型的零值。从一个已关闭的 channel 中读取消息永远不会阻塞，并且会返回一个为 false 的 ok-idiom，可以用它来判断 channel 是否关闭
	关闭 channel 会产生一个广播机制，所有向 channel 读取消息的 goroutine 都会收到消息

	作者：不智鱼
	链接：https://www.jianshu.com/p/24ede9e90490
	来源：简书
	简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。
	*/
	
	close(ch) // 关闭channel（不能重复关闭）

  fmt.Println(result)
}
