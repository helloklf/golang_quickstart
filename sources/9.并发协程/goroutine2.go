package main

// 这个例子用于观测Go协程的调度机制

import (
	"fmt"
  "time"
  "runtime"
)

func Compute() {
  var total = 1
  for index := 1; index < 5; index++ {
    fmt.Println("Compute")
    for index2 := 1; index2 < 2000000000; index2++ {
      total += (total / index)
    }
  }
  fmt.Println("Compute goroutine")
}

func Compute2() {
  var total = 1
  for index := 1; index < 5; index++ {
    fmt.Println("Compute2")
    for index2 := 1; index2 < 2000000000; index2++ {
      total += (total / index)
    }
  }
  fmt.Println("Compute2 goroutine")
}

func HelloWorld1() {
  var total = 1
  for index := 1; index < 5; index++ {
    fmt.Println("HelloWorld1")
    for index2 := 1; index2 < 2000000000; index2++ {
      total += (total / index)
    }
  }
  fmt.Println("Hello world goroutine")
}

func main() {
  runtime.GOMAXPROCS(1) // 为了方便观察协程之间的切换，设置只用一个线程跑协程

  go Compute2() // 创建 goroutine
  go Compute() // 创建 goroutine
  go HelloWorld1() // 创建 goroutine
  
  // Compute2() // 在主线程上运行
  // Compute() // 在主线程上运行
  // HelloWorld1() // 在主线程上运行

  time.Sleep(10 * time.Second) // 为了确保 HelloWorld执行完之前程序不要退出，这里让主goroutine先等待一会儿！
  fmt.Println("********")
}