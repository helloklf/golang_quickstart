package main

import (
	"fmt"
	"time"
)

func HelloWorld() {
  fmt.Println("Hello world goroutine")
}

func main() {
  go HelloWorld() // 创建 goroutine
  time.Sleep(2 * time.Second) // 为了确保 HelloWorld执行完之前程序不要退出，这里让主goroutine先等待一会儿！
  fmt.Println("********")
}