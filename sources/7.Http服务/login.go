package main

import "net/http"
import "encoding/json"

func main() {
	http.HandleFunc("/login", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == "GET" {
			writer.Write([]byte("请用POS请求！"))
		} else {
			// 必须要调用的，解析Form参数
			request.ParseForm()

			var result map[string]interface{}
			result = make(map[string]interface{})
	
			// request.FormValue("username") 和 request.Form["username"] 得到的结果并不同
			// 后者将得到一个Array，而前者只会得到一个值
			result["username"] = request.FormValue("username")
			result["password"] = request.FormValue("password")
	
			// 序列化Map为JSON字符串
			data,err := json.Marshal(result)

			if (err == nil) {
				writer.Write([]byte(data))
			}
		}
	})

	http.ListenAndServe(":9090", nil)
}