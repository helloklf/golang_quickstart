// 包名（命名空间）
package main

// 依赖
import "fmt"

type Callback func(result string)

func Download(onSuccess Callback, onError Callback) {
	onSuccess("下载了100MB")
	onError("下载中断...")
}

// 入口函数
func main() {
	Download(func(result string) {
		fmt.Println("操作成功：" + result)
	}, func(message string) {
		fmt.Println("下载出错：" + message)
	})
}
