// 包名（命名空间）
package main

// 依赖
import "fmt"

// 人
type People interface {
	// 叫你吃多少，你能不能吃？
	eat(count int) bool
	// 叫你跑多远，能不能跑？
	run(distance int) bool
	// 叫你读多少次，你读了几次？
	repeater(frequency int) int
}

// 男人
type Man struct {
}

func (man Man) eat(count int) bool {
	return count < 2000
}
func (man Man) run(distance int) bool {
	return distance < 8000
}
func (man Man) repeater(frequency int) int {
	return frequency // 你说几次就几次呗
}

type Woman struct {
}

func (woman Woman) eat(count int) bool {
	return count < 2500 // 设定女人可以吃2500g
}
func (woman Woman) run(distance int) bool {
	return distance < 4000 // 设定女人可以跑4000米
}
func (woman Woman) repeater(frequency int) int {
	return frequency // 你说几次就几次呗
}

// 入口函数
func main() {
	var man Man
	fmt.Println("男人，跑100米，小意思啦", man.run(100))
	fmt.Println("男人，跑10千米试试？", man.run(10000))

	var woman Woman
	fmt.Println("女人，吃它个两公斤试试", woman.run(100))
	fmt.Println("复读机无所畏惧", woman.repeater(10000))
}
