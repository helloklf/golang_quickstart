// 包名（命名空间）
package main

// 依赖
import "fmt"

// 入口函数
func main() {
	var onSuccess = func(result string) {
		fmt.Println("操作成功：" + result)
	}
	onSuccess("下载了100MB")
}
