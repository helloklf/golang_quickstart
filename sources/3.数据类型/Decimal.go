package main

import "fmt"
import "github.com/shopspring/decimal"

func main() {
	a := 1.69
	b := 1.7

	a1 := decimal.NewFromFloat(a)
	b1 := decimal.NewFromFloat(b)
	c1 := a1.Mul(b1)
	fmt.Println(c1)  // 输出 2.873
}