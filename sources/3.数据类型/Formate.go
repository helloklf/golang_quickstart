// 包名（命名空间）
package main

// 依赖
import (
	"fmt"
	"strconv"
)

// 入口函数
func main() {
	// fmt.Println("0" + 10) // 错误
	// fmt.Println("0" + strconv.FormatInt(10, 10)) // 正确

	// 按16进制格式化数字成字符串
	var str5 = strconv.FormatInt(4095, 16)
	fmt.Println(str5) // fff

	// 按10进制格式化数字成字符串
	var str6 = strconv.FormatInt(4095, 10)
	fmt.Println(str6) // 4095

	var str7 = strconv.FormatBool(false)
	fmt.Println(str7) // false
}
