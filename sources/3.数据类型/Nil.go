package main

import "fmt"

func main(){
    var value interface{}
    if value == nil {
        fmt.Println("val is nil")
    } else {
        fmt.Println("val is not nil")
    }
}