package main
import "fmt"

func main() {
	// 浮点计算有明显的精度问题
	a := 1.69
	b := 1.7
	c := a * b // 正确结果应是 2.873
	fmt.Println(c) // 实际结果是 2.8729999999999998
}