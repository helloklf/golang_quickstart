// 包名（命名空间）
package main

// 依赖
import (
	"fmt"
	"strconv"
)

// 入口函数
func main() {
	var value int = 100
	// int转位float32，自然是没问题的
	fmt.Println(float32(value))
	// float32转float64自然也是没问题的
	// 当然，float毕竟不是精准的类型，转换后值可能出现一些误差
	var floatValue = float32(100.11)
	fmt.Println(float64(floatValue))

	// 这肯定要报错了，100.1并不是个有效的整数
	// fmt.Println(int(100.1))


	// 字符串和数字转换

	// 识别为10进制
	var intValue, error = strconv.ParseInt("010", 10, 32)
	if (error != nil){
		fmt.Println("转换失败！")
	} else {
		fmt.Println(intValue) // 10
	}

	// 识别为8进制
	var intValue2, error2 = strconv.ParseInt("010", 8, 32)
	if (error2 != nil){
		fmt.Println("转换失败！")
	} else {
		fmt.Println(intValue2) // 8
	}

	// 识别为16进制（注意：0xFFF并不能转换，请去掉0x直接传入FFF）
	var intValue3, error3 = strconv.ParseInt("FFF", 16, 32)
	if (error3 != nil){
		fmt.Println("转换失败！")
	} else {
		fmt.Println(intValue3) // 8
	}

	var floatValue4, error4 = strconv.ParseFloat("120.999", 32)
	if (error4 != nil){
		fmt.Println("转换失败！")
	} else {
		fmt.Println(floatValue4) // 120.999...
	}
}
