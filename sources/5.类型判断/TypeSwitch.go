// 包名（命名空间）
package main

// 依赖
import "fmt"

// 人
type People interface {
	// 叫你吃多少，你能不能吃？
	eat(count int) bool
	// 叫你跑多远，能不能跑？
	run(distance int) bool
	// 叫你读多少次，你读了几次？
	repeater(frequency int) int
}

// 男人
type Man struct {
	age int
}

func (man Man) eat(count int) bool {
	return count < 2000
}
func (man Man) run(distance int) bool {
	return distance < 8000
}
func (man Man) repeater(frequency int) int {
	return frequency // 你说几次就几次呗
}

type Woman struct {
	age int
}

func (woman Woman) eat(count int) bool {
	return count < 2500 // 设定女人可以吃2500g
}
func (woman Woman) run(distance int) bool {
	return distance < 4000 // 设定女人可以跑4000米
}
func (woman Woman) repeater(frequency int) int {
	return frequency // 你说几次就几次呗
}

func getPeoples() [3]interface{} {
	var peoples [3]interface{}
	peoples[0] = Man{age: 18}
	peoples[1] = Woman{age: 20}
	peoples[2] = "搞事起"

	return peoples
}

// 入口函数
func main() {
	var peoples = getPeoples()

	//for index,people := range peoples {
	// 如果你定义了index又不使用，那么编译时会报错。所以，将需要忽略的参数写成“_”即可解决问题。这用法kotlin语言也是一致的
	for _, people := range peoples {
		/*
			// 如果你需要得到type
			switch typeInfo := people.(type) {
				case Man:
					fmt.Printf("Man\n")
				case Woman:
					fmt.Printf("Woman\n")
				default:
					fmt.Printf("其它类型")
			}
		*/
		// 如果你只是需要判断类型
		switch people.(type) {
		case Man:
			fmt.Printf("这个是 Man\n")
		case Woman:
			fmt.Printf("这个是 Woman\n")
		default:
			fmt.Printf("其它类型")
		}
	}
}
