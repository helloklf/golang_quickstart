package main

import "fmt"

func modifyArra(arr [5]int) {
	arr[2] = 100 // 修改item2的值
}

func main() {
	// 数组是值类型
	arr := [5]int{ 1, 2, 3, 4, 5 }
	modifyArra(arr)
	fmt.Printf("item %d 值为: %d\n", 2, arr[2])

	// 数组声明语法
	var arr2 [5]int
	fmt.Printf("item %d 值为: %d\n", 2, arr2[2]) // item 2 值为: 0

	var arr3 [5]int = [5]int{ 1, 2, 3, 4, 5 }
	fmt.Printf("item %d 值为: %d\n", 2, arr3[2]) // item 2 值为: 3

	var arr4 = [5]int{ 1, 2, 3, 4, 5 }
	fmt.Printf("item %d 值为: %d\n", 2, arr4[2]) // item 2 值为: 3

	var arr5 = [...]int{ 1, 2, 3, 4, 5 }
	fmt.Printf("item %d 值为: %d\n", 2, arr5[2]) // item 2 值为: 3

	var arr6 = [...]int{2: 50, 5: 100}
	fmt.Printf("item %d 值为: %d\n", 2, arr6[2]) // item 2 值为: 50
}