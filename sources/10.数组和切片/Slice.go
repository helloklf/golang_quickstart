package main

import "fmt"

func modifySlice(slice []int) {
	slice[2] = 100 // 修改item2的值
}

// 切片的原理
func principle()  {
	// 声明一个长度为 5 的数组，并为元素赋值
	var data = [5]int{ 1, 2, 4, 5, 6 }
	// 截取数组data索引 3 到 5 的部分作为切片
	var mySlice = data[3:5]

	// 那么，访问 mySlice[0] 和 data[3] 得到的值是一致的
	if (mySlice[0] == data[3]) {
		fmt.Printf("data[3] = %d，mySlice[0] = %d\n", data[3], mySlice[0]) // data[3] = 5，mySlice[0] = 5
	}

	// 同样，通过mySlice修改数据，data也会跟着变
	mySlice[1] = 100
	if (mySlice[0] == data[3]) {
		fmt.Printf("data 的数据 %v\n", data) // data 的数据 [1 2 4 5 100]
	}
}

func main() {
	// 切片是引用类型
	slice1 := []int{ 1, 2, 3, 4, 5 }
	modifySlice(slice1)
	fmt.Printf("slice0 %v\n", slice1) // [1 2 100 4 5]


	// 切片的原理
	principle()
		

	// 切片声明语法
	var slice2 []int
	fmt.Printf("slice2 %v\n", slice2) // []
	var slice3 = []int{ 1, 2, 3, 4, 5 }
	fmt.Printf("slice3 %v\n", slice3) // [1 2 3 4 5]

	// 创建方式make([]type, len, cap)，其中，type表示数组元素类型，len表示长度，cap表示容量（可不传）
	slice4 := make([]int, 3, 5)
	fmt.Printf("slice4 %v\n", slice4) // [0 0 0]

	// 对切片重新切片 用法：slice[startIndex:endIndex]
	sliceData := []int{ 2, 4, 6, 8, 10 }
	slice5 := sliceData[1:3]
	fmt.Printf("slice5 %v\n", slice5) // [4 6]
	slice6 := sliceData[1:]
	fmt.Printf("slice6 %v\n", slice6) // [4 6 8 10]
	slice7 := sliceData[:3]
	fmt.Printf("slice7 %v\n", slice7) // [2 4 6]
	slice8 := sliceData[:]
	fmt.Printf("slice8 %v\n", slice8) // [2 4 6 8 10]

	// 对数组进行切片 用法：array[startIndex:endIndex]
	arrData := [...]int{ 1, 2, 3, 4, 5 }
	slice9 := arrData[1:4]
	fmt.Printf("slice9 %v\n", slice9) // [2 3 4]

	// 获取切片长度和容量
	sliceData2 := make([]int, 3, 5)
	fmt.Printf("sliceData2 长度 %d\n", len(sliceData2))
	fmt.Printf("sliceData2 容量 %d\n", cap(sliceData2))
}