package main

import "fmt"

func main() {
	// 获取切片长度和容量
	sliceData2 := make([]int, 3, 5)
	fmt.Printf("sliceData2 长度 %d\n", len(sliceData2))
	fmt.Printf("sliceData2 容量 %d\n", cap(sliceData2))

	var slice1 = make([]int, 5, 10) // 声明切片，长度5，容量10
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice1), cap(slice1), slice1) // 长度:5, 容量:10, 数据:[0 0 0 0 0]
	// os[6] = 1 // 索引6不可访问，panic: 索引超出范围

	slice2 := slice1[:8] // 重切片，切取 索引0到8(不含8)
	slice2[6] = 999 // 重切片后，索引6可以访问了
	slice2[7] = 1000
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice2), cap(slice2), slice2) // 长度:5, 容量:10, 数据:[0 0 0 0 0 0 999 1000]

	slice3 := slice2[5:7] // 再次重切片，缩减长度，切取 索引5到7(不含7)
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice3), cap(slice3), slice3) // 长度:2, 容量:5, 数据:[0 999]

	// append 用法
	// 使用"append(slice, items...)"向slice添加若干个值，并返回一个新的切片（不会改变原切片数据）<br />
	// 当数据长度超过原slice容量时，会自动调整新slice容量
	slice4 := make([]int, 0, 1)
	slice5 := append(slice4, 10086, 10010, 114)
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice4), cap(slice4), slice4) // 长度:0, 容量:1, 数据:[]
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice5), cap(slice5), slice5) // 长度:1, 容量:4, 数据:[10086 10010 114]

	// copy 用法
	// 使用"copy(targetSlice, sourceSlice)"，将一个切片的数据复制到另一个切片<br />
	slice6 := make([]int, 2, 5)
	slice6[0] = 10
	slice6[1] = 20
	slice7 := []int{4, 5, 6}
	var count = copy(slice6, slice7)
	if (count < 3) {
		fmt.Printf("没全部完成，只复制了：%d 个\n", count) // 没全部完成，只复制了：2 个
	}
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice6), cap(slice6), slice6) // 长度:2, 容量:5, 数据:[4, 5]


	slice8 := []int{10, 20, 30, 40, 50}
	slice9 := []int{9, 99, 999}
	slice10 := slice8[2:5]
	var count2 = copy(slice10, slice9)
	if (count < 3) {
		fmt.Printf("没全部完成，只复制了：%d 个\n", count2) // 没全部完成，只复制了：3 个
	}
	fmt.Printf("长度:%d, 容量:%d, 数据:%v\n", len(slice8), cap(slice8), slice8) // 长度:5, 容量:5, 数据:[10 20 9 99 999]
}