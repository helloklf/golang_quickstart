// 包名（命名空间）
package main

// 依赖
import "fmt"

// 入口函数
func main() {
	fmt.Println("Hello, World!")
}
