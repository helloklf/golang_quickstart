## 开始之前
- 设备：准备好一台Linux、OSX或Windows系统的电脑
- 基础：至少熟悉任意一门编程语言

## 内容
- Golang 开发简介和开发环境准备
- Golang 基本语法
- Golang 创建Web服务器
- Golang 使用Mongodb

## 期望目标
- 了解Golang是个啥有啥用
- 大致了解Golang的语法
- 创建Golang + Mongodb的Web服务
- 在Docker上部署Web服务
- 上面是瞎说的，目标只有：
- 掌握一门新的“Hello World！”写法